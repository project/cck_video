INSTALLATION
------------
Installation procedure contains 2 parts:
a. Install Module
b. Install Player


INSTALL MODULE
--------------
Same as other Drupal module

INSTALL PLAYER
--------------

JW Player:
1. Download: http://www.longtailvideo.com/jw/upload/mediaplayer-viral.zip
2. Upload your player file to sites/all/libraries/
3. extract and rename mediaplayer-5.9-viral to mediaplayer
4. player file: sites/all/libraries/mediaplayer/player.swf

Flow Player:
1. Download: http://releases.flowplayer.org/flowplayer/flowplayer-3.2.8.zip
2. Upload your player file to sites/all/libraries/
3. extract become: flowplayer
4. player file: sites/all/libraries/flowplayer/flowplayer-3.2.8.swf


