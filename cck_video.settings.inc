<?php
/**
 * @file
 * Settings page callback file for the cck_video module.
 */

/**
 * Menu callback;
 */
 
function cck_video_admin_settings() {
  $form = array();
  // only administrators can access this function
  
  // Generate the form - settings applying to all patterns first
  $form['cck_video_settings'] = array(
    '#type' => 'fieldset',
    '#weight' => -30,
    '#title' => t('Basic settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE    
  );
  
  $form['cck_video_settings']['cck_video_player'] = array(
		'#type' => 'select',
		'#title' => t('Player Name'),
		'#default_value' => variable_get('cck_video_player', 'JW Player'),
		'#options' => array('JW Player' => 'JW Player', 'Flow Player' =>'Flow Player'),
	);
	
  return system_settings_form($form);
}

